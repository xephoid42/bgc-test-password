package org.blackgirlscode.bgctestpassword;

import org.junit.Test;

import static org.junit.Assert.*;

public class PasswordUtilUnitTests {

    @Test
    public void validPassword() {
        assertTrue(PasswordUtil.validatePassword("aaBB11$$"));
    }

    @Test
    public void noSpaces() {
        assertFalse(true);
    }

    @Test
    public void atLeast8Characters() {
        assertFalse(true);
    }

    @Test
    public void atLeast1Uppercase() {
        assertFalse(true);
    }

    @Test
    public void atLeast1Lowercase() {
        assertFalse(true);
    }

    @Test
    public void atLeast1Number() {
        assertFalse(true);
    }

    @Test
    public void atLeast1SpecialCharacter() {
        assertFalse(true);
    }
}