package org.blackgirlscode.bgctestpassword;

/**
 * Created by zekes on 7/11/2017.
 */

public class PasswordUtil {

    public static final String PASSWORD_REGEX = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";

    /**
     * Returns true if password meets the following criteria:
     * (?=.*[0-9]) a digit must occur at least once
     * (?=.*[a-z]) a lower case letter must occur at least once
     * (?=.*[A-Z]) an upper case letter must occur at least once
     * (?=.*[@#$%^&+=]) a special character must occur at least once
     * (?=\\S+$) no whitespace allowed in the entire string
     * .{8,} at least 8 characters
     *
     * @param password
     */
    public static final boolean validatePassword(String password) {
        return password.matches(PASSWORD_REGEX);
    }
}
